lazy val root = (project in file(".")).
  settings(
    name := "RedisTest",
    version := "1.0",
    scalaVersion := "2.13.2",
    unmanagedResourceDirectories in Compile += { baseDirectory.value / "src/main/resources" }
  )


libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.20"
)


