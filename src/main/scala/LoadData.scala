import com.redis._

import scala.io.Source
import scala.sys.process._
import scala.util.Try

object LoadData extends App {

  ("docker pull redis").!

  ("docker run -d --name redis-aof -p 8080:6379 redis --appendonly yes").!

  ("docker run -d --name redis-rdb -p 8081:6379 redis").!

  val data = Source.fromResource("10mb.txt").getLines.mkString

  println(data.length)

  val rAOF = new RedisClient("localhost", 8080)

  val rRDB = new RedisClient("localhost", 8081)

  Thread.sleep(5000)

  println("Start to load data")

  (1 to 100).foreach(x => {
    rAOF.set(x, data)
    rRDB.set(x, data)
  })

  println("Finish to load data")

  rRDB.save

  Try(rAOF.shutdown)
  Try(rRDB.shutdown)

  println("Start to restart after shutdown")

  ("docker start -i redis-rdb" #> new java.io.File("RDB.txt")).lazyLines

  Thread.sleep(5000)

  ("docker start -i redis-aof" #> new java.io.File("AOF.txt")).lazyLines

  Thread.sleep(5000)

  println("Please check files")
}
